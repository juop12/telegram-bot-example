require 'spec_helper'

require "#{File.dirname(__FILE__)}/../app/movie"

describe 'Movie' do
  it 'should get 11 Oscars won, 126 wins and 83 nominations total from movie with movie_name Titanic' do
    movie_name = 'Titanic'
    api_key = ENV['OMDB_API_KEY']

    when_i_hear_answer_from_omdb(movie_name, 'Won 11 Oscars. 126 wins & 83 nominations total')
    movie = Movie.new(movie_name, api_key)

    expect(movie.awards).to eq 'Won 11 Oscars. 126 wins & 83 nominations total'
  end

  it 'should raise MovieNotFound from movie that does not exist named Not Existant Movie' do
    movie_name = 'Not Existant Movie'
    api_key = ENV['OMDB_API_KEY']

    when_i_hear_error_from_omdb(movie_name, 'Movie not found!')

    expect { Movie.new(movie_name, api_key) }.to raise_error(MovieNotFound)
  end
end
