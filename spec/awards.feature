@awards
Feature: Awards

  Scenario: a1 - awards when query for the movie Titanic
    Given /awards "Titanic" is sent
    Then the movie "Titanic" Won 11 Oscars
    And 126 wins
    And 83 nominations total

  Scenario: a2 - awards when query for the movies Titanic and Toy Story
      Given /awards "Titanic" and "Toy Story" is sent
      Then the movie "Titanic" Won 11 Oscars
      And 126 wins
      And 83 nominations total
      And the movie "Toy Story" Nominated for 3 Oscars
      And 29 wins
      And 23 nominations total

  Scenario: a3 - awards when query for the movie Terminator that has no awards
    Given /awards "Terminator" is sent
    Then the movie "Terminator" has no awards

  Scenario: a4 - awards when query for a movie that does no exist
    Given /awards "Not Existant Movie" is sent
    Then the movie "Not Existant Movie" does not exist