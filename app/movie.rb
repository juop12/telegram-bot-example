class MovieNotFound < StandardError
end

class Movie
  attr_reader :awards

  API_URL = 'https://www.omdbapi.com/'.freeze

  def initialize(movie_name, api_key)
    response = Faraday.get(API_URL, { t: movie_name, apikey: api_key })
    response_json = JSON.parse(response.body)
    raise MovieNotFound, "The movie #{movie_name} was not found" if response_json['Response'] == 'False'

    @awards = response_json['Awards']
  end
end
